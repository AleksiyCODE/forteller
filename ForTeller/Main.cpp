#include <stdio.h>
#include<iostream>
#include <vector>
#include <algorithm>
#include<iomanip>
#include <Windows.h>


struct PredictionProbability
{
	size_t prediction;
	size_t matches;
	size_t kernel;
};

typedef std::vector<size_t> sequence;
size_t CalculateOptimalKernel(const sequence& seq);
std::pair<size_t, size_t> PredictCharacter(const sequence& sequ, size_t kernel);		//first - prediction, second - matches
size_t AnalysePredictions(const std::vector<PredictionProbability>& pred);


//globals
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);



int main()
{
	sequence seq;
	std::vector<PredictionProbability>nextCharacterAnal;
	seq.reserve(100);
	int i;
	do
	{
		i = getchar();
		seq.push_back(i-'0');
	} while (i == '0' || i == '1');
	seq.pop_back();
	size_t initialLen = seq.size();

	const auto kernel = CalculateOptimalKernel(seq);
	nextCharacterAnal.resize(kernel);
	for (size_t elementsToPredict = 0u; elementsToPredict < kernel/3u; elementsToPredict++)
	{
		for (size_t i = 2; i < kernel; ++i)
		{
			auto prediction = PredictCharacter(seq, i);
			nextCharacterAnal.push_back(PredictionProbability{prediction.first,prediction.second, i });
		}
		seq.push_back(AnalysePredictions(nextCharacterAnal));
		nextCharacterAnal.clear();
	}

	SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	for (size_t i = 0; i < initialLen; i++)
	{
		std::cout << seq[i];
	}
	SetConsoleTextAttribute(hConsole, BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
	for (size_t i = initialLen; i < seq.size(); i++)
	{
		std::cout << seq[i];
	}
	SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	int k = 0;

	system("pause");

	return 0;
}

size_t CalculateOptimalKernel(const sequence& seq)
{
	if (seq.size() > 10u) return seq.size() / 3u; else return 3u;
}

std::pair<size_t, size_t> PredictCharacter(const sequence& seq, size_t kernel)
{





	size_t patternsFound0 = 0;
	size_t patternsFound1 = 0;
	auto occur = std::search(seq.begin(), seq.end()-1, seq.end() - kernel, seq.end());
	while (occur <= seq.end()-kernel)
	{
		if (*(occur + kernel) == 0)patternsFound0++; else patternsFound1++;






	//	for (auto k = seq.begin(); k < occur; k++)
	//	{
	//		std::cout << *k;
	//	}	
	//
	//SetConsoleTextAttribute(hConsole, BACKGROUND_BLUE| BACKGROUND_GREEN| BACKGROUND_RED);
	//	for (auto k = occur; k < occur + kernel; k++)
	//	{
	//		std::cout << *k;
	//	}	
	//
	//	SetConsoleTextAttribute(hConsole, BACKGROUND_GREEN | BACKGROUND_RED);
	//		std::cout << *(occur + kernel);					//next character after the sequence being analysed
	//
	//SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE| FOREGROUND_GREEN| FOREGROUND_RED);
	//	for (auto k = occur + kernel+1; k < seq.end()-kernel; k++)
	//	{
	//		std::cout << *k;
	//	}
	//
	//SetConsoleTextAttribute(hConsole, BACKGROUND_RED);
	//	for (auto k = seq.end() - kernel; k < seq.end(); k++)
	//	{
	//		if(k> occur + kernel)
	//			std::cout << *k;
	//	}
	//
	//SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE| FOREGROUND_GREEN| FOREGROUND_RED);
	//	std::cout << std::endl;



		++occur;
		occur = std::search(occur, seq.end()-1, seq.end() - kernel, seq.end());
	}
	if (patternsFound0 > patternsFound1)
		return { 0,patternsFound0 };
	else
		return { 1,patternsFound1 };
}

size_t AnalysePredictions(const std::vector<PredictionProbability>& pred)
{
	float prob0 = 0.0f;
	float prob1 = 0.0f;
	for (auto& p : pred)
	{
		if (p.prediction == 0)
			prob0 += 2*p.kernel * (int)log(p.kernel)   * p.matches;
		else
			prob1 += 2*p.kernel * (int)log(p.kernel)   * p.matches;
	}
	if (prob0 > prob1)
		return 0;
	else
		return 1;
}
